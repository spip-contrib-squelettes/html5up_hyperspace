<?php
// This is a SPIP language file  --  Ceci est un fichier langue de SPIP

if (!defined('_ECRIRE_INC_VERSION')) {
	return;
}

$GLOBALS[$GLOBALS['idx_lang']] = array(
	'html5up_hyperspace_description' => '',
	'html5up_hyperspace_nom' => 'Html5up Hyperspace',
	'html5up_hyperspace_slogan' => 'Squelette responsive «Hyperspace» de HTML5UP',
);
