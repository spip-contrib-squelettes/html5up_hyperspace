# Changelog

## Unreleased

### Fixed
- Réparer l'ordre d'affichage des articles pour la rubrique feature
- Ne pas chercher le champ chapo des rubriques
- Mieux cibler les sections pour ne pas cibler la pagination dans les rubriques
- La liste des articles de la rubrique ne doit pas avoir de couleur de fond spécifique
- Utiliser les bonnes couleurs pour que la pagination soit visible

### Feature
 - Passer les couleurs du thème en variables pour faciliter les surcharges
 - Une composition rubrique qui affiche les articles sous forme de grille

### Changed
 - Compatibilité minimum SPIP 4.1

## v3.1.3 - 2023-02-27

### Changed
 - Compatibilité SPIP 4.2
