<?php

// Sécurité
if (!defined('_ECRIRE_INC_VERSION')) {return;}

// Intertitres commençant par h2
$GLOBALS['debut_intertitre'] = "\n<h2 class=\"spip\">\n";
$GLOBALS['fin_intertitre'] = "</h2>\n";
// Activer html5 depuis le plugin
$GLOBALS['meta']['version_html_max'] = 'html5';